#!/bin/bash -x

# Run this script to launch new gitlab runner instance in rackspace for samba.
# You need env vars sourced for `rack`, and vault password added in ~/.vault_password_rackspace.
# vault password for rackspace could be found in pview

./rackspace.yml -v \
    --vault-password-file ~/.vault_password_rackspace \
    -e ENV_NAME=gitlab-runner \
    -e MACHINE_DRIVER=rackspace \
    -e RUNNER_NAME=gitlab-runner-rackspace-$(date +%m%d) \
    "$@"
