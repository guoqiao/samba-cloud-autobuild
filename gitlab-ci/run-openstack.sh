#!/bin/bash -x

export ANSIBLE_INVENTORY=../inventory/openstack_inventory.py

./openstack.yml -v \
    --vault-password-file ~/.vault_password_autobuild \
    -e ENV_NAME=gitlab-runner \
    -e MACHINE_DRIVER=openstack \
    -e RUNNER_NAME=gitlab-runner-openstack-$(date +%m%d) \
    "$@"
