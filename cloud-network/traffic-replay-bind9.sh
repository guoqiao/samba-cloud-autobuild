#!/bin/bash

# run traffic replay with BIND9_DLZ as dns backend in dns mode
# provision dc with internal dns first, then upgrade dns

./traffic-replay.yml -v \
    -e ENV_NAME=bind9 \
    -e primary_dc_name=bind9-dc0 \
    -e samba_repo_version=master \
    -e samba_backup_file=$HOME/backup/samba-backup-docker-mdb-5000-max-5000-with-dns-0411.tar.bz2 \
    -e samba_dns_backend=BIND9_DLZ \
    -e samba_traffic_mode=dns \
    -e run_cleanup=true \
    -e run_replay=true \
    -e run_summary=true \
    "$@"
