#!/bin/bash

# create a samba domain and restore 125k users.

set -xue

./samba-domain.yml -v \
    -e ENV_NAME=r125k \
    -e samba_backup_file=$HOME/backup/samba-backup-docker-mdb-125000-max-125000.tar.bz2 \
    "$@"
