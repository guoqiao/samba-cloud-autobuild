#!/bin/bash

set -xue

./samba-domain.yml -v \
    -e ENV_NAME=r50k \
    -e primary_dc_name=r50k-dc0 \
    -e samba_backup_file=$HOME/backup/samba-backup-docker-mdb-50000-max-50000.tar.bz2 \
    "$@"
