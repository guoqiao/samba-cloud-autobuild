# ansible-roles

This directory should contains the ansible roles which could be reused by samba.

## roles principal
- each role should try to be self-contained as much as possible
- keep tasks and variables minimal
- provide reasonable default value for vars if possible
- define a `required_vars` for mandatory vars and check them in role

