"""These bits are shared between start-samba and multi-samba"""
import os
import sys
import subprocess
import random
import re

try:
    from pipes import quote
except ImportError:
    from shlex import quote


HERE = os.path.dirname(__file__)
YAML_DIR = os.path.join(HERE, 'yaml')

OPENRC_TABLE = {
    # listing both our short names, and the official names in case we
    # ever use those.
    'por':      '~/sambatest.catalyst.net.nz-openrc.sh',
    'nz-por-1': '~/sambatest.catalyst.net.nz-openrc.sh',
    'wlg':      '~/sambatest.catalyst.net.nz-openrc-wlg.sh',
    'nz_wlg_2': '~/sambatest.catalyst.net.nz-openrc-wlg.sh'
}

DEFAULT_REGION = os.environ.get('OS_REGION_NAME', 'wlg')

class SambaCloudError(Exception):
    pass


def get_credentials(region, no_secrets=False):
    """Get the credentials for the region ('wlg' or 'por')"""
    credentials_file = OPENRC_TABLE[region]
    credentials = {
        "OS_AUTH_URL": '',
        "OS_TENANT_ID": '',
        "OS_TENANT_NAME": '',
        "OS_USERNAME": '',
        "OS_PASSWORD": '',
        "OS_INTERFACE": '',
        "OS_IDENTITY_API_VERSION": '',
        "OS_REGION_NAME": '',
        "OS_PROJECT_ID": '',
        "OS_PROJECT_NAME": '',
        "OS_USER_DOMAIN_NAME": '',
        "OS_PROJECT_DOMAIN_ID": '',
        'SMTP_USERNAME': '',
        'SMTP_PASSWORD': '',
        'SMTP_SERVER': '',
    }
    if no_secrets:
        # There is no point preserving any of the credential
        # variables, even though most are not really secret -- they
        # are only useful in conjunction with the password.
        return credentials

    env = subprocess.check_output(['bash', '-c', 'source %s && env' %
                                   credentials_file]).decode('utf-8')

    for line in env.split("\n"):
        try:
            k, v = line.strip().split('=', 1)
        except ValueError:
            continue
        if k in credentials:
            credentials[k] = v

    return credentials


def get_package_list(name, form=str):
    fn = os.path.join(HERE, 'package-lists', name)
    f = open(fn)
    if form is str:
        packages = f.read()
    elif form is list:
        packages = [x[3:].strip() for x in f if x[:3] == ' - ']
    f.close()
    return packages


OPENSTACK_COMMANDS = {
    'flavor_list': ['openstack', 'flavor', 'list'],
    'image_list': ['openstack', 'image', 'list'],
    'network_list': ['openstack', 'network', 'list', '--internal']
}


def run_openstack_cmd(cmd, region=DEFAULT_REGION, dry_run=False):
    if cmd not in OPENSTACK_COMMANDS:
        raise SambaCloudError("Unknown openstack command option: %s" % cmd)

    open_rc = OPENRC_TABLE[region]
    args = ['.', open_rc, ';'] + OPENSTACK_COMMANDS[cmd]
    str_args = ' '.join(args)
    if dry_run:
        return str_args
    return subprocess.check_output(str_args, shell=True)


def add_common_args(parser):
    parser.add_argument('--whole-name', action='store_true',
                        help="use the 'suffix' as the whole name")

    parser.add_argument('--key-name', default=os.environ.get('USER'),
                        help="use this ssh key pair")

    parser.add_argument('-i', '--image', default="samba-build-14.04-template",
                        help="specify the image to use")

    parser.add_argument('-f', '--flavor', default='c1.c4r8',
                        help=("specify which flavour to use "
                              "(default: 4 CPUs, 8GB)"))

    parser.add_argument('--region', default=DEFAULT_REGION,
                        help="'wlg' or 'por' (default '%s')" % DEFAULT_REGION)

    parser.add_argument('-b', '--branch',
                        help="git branch to use")

    parser.add_argument('-r', '--remote',
                        default='git://git.catalyst.net.nz/samba.git',
                        help="git remote to use")

    parser.add_argument('--image-list', action='store_true',
                        help="list the available images, and exit")

    parser.add_argument('--flavor-list', action='store_true',
                        help="list the available flavours, and exit")

    parser.add_argument('-n', '--dry-run', action='store_true',
                        help="do nothing remotely, "
                        "show the command that would be run")

    parser.add_argument('-v', '--verbose', action='store_true',
                        help="say a litle more about what is happening")

    parser.add_argument('--onfail', default="delete",
                        choices=['suspend', 'shelve', 'continue',
                                 'delayed-delete', 'delete'],
                        help=("Choose an action to run when the autobuild fails"))

    parser.add_argument('--readahead', default=8192, type=int,
                        help=("block device readahead (default 8192)"))

    parser.add_argument('--maxtime', default=3600 * 6, type=int,
                        help=("how long to wait before timing out"))

    parser.add_argument('--tmpfs', action='store_true',
                        help=("run autobuild in a tmpfs (use 8GB RAM)"))

    parser.add_argument('--ramfs', action='store_true',
                        help=("run autobuild in a ramfs (use 8GB RAM)"))

    parser.add_argument('--skip-samba-build', action='store_true',
                        help=("stop before building samba, do not autobuild"))

    parser.add_argument('--skip-samba-autobuild', action='store_true',
                        help=("prepare the image for autobuild, then stop"))

    parser.add_argument('--no-secrets', action='store_true',
                        help=("don't upload openstack password; instance won't "
                              "delete/suspend/shelve itself"))


def process_common_args(args):
    if args.dry_run:
        print " This is what we WOULD be doing without -n/--dry-run:\n"

    for openstack_cmd in ("image_list", "flavor_list"):
        if vars(args)[openstack_cmd]:
            print(run_openstack_cmd(openstack_cmd,
                                    region=args.region,
                                    dry_run=args.dry_run))
            sys.exit()

    if args.branch is None:
        print "You have not specified a branch!"
        print "Use '-b master' if you want the master branch."
        sys.exit()

    try:
        repo_check_call = ['git', 'ls-remote', '--heads', '--exit-code', args.remote, args.branch]
        subprocess.check_output(repo_check_call)
    except:
        print "ERROR: Specified git repository or branch does not exist."
        raise

def sanitise_hostname(hostname):
    fixed_name = re.sub(r'[^a-z0-9-]+', '-', hostname.lower())
    if fixed_name != hostname:
        print >> sys.stderr, ("WARNING: the host name '%s' contains invalid "
                              "characters. Using '%s' instead." %
                              (hostname, fixed_name))
    return fixed_name
