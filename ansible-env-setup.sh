#!/bin/bash
set -xue

sudo apt-get install --yes python3-dev python3-setuptools python3-pip
sudo pip3 install -U -r requirements.txt
/usr/local/bin/ansible-galaxy install --force -r requirements.yml
/usr/local/bin/ansible-galaxy list --verbose
./ansible-roles-clone-or-update.yml
